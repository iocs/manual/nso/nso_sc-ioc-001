require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

#--------
dbLoadRecords("$(E3_CMD_TOP)/db/opioc.template",      "P=NSO:, R=Ops:")
dbLoadRecords("$(E3_CMD_TOP)/db/opioc-ts2.template",  "R=Ops:, TS2=TS2:")

# some other contacts and details
dbLoadRecords("$(E3_CMD_TOP)/db/opioc-pos.template",  "P=NSO:, R=Ops:")
